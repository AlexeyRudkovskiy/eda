# Food delivery app

## Android

<p>
    <a><img src="https://gitlab.com/AlexeyRudkovskiy/eda/badges/Android/build.svg" alt="build status"></a>
    <a href="https://gitlab.com/AlexeyRudkovskiy/eda/commits/Android"><img alt="coverage report" src="https://gitlab.com/AlexeyRudkovskiy/eda/badges/Android/coverage.svg" /></a>
</p>

## iOS

<p>
    <a href='http://194.110.6.97:9090/job/ProjectFood/'><img src='http://194.110.6.97:9090/job/ProjectFood/badge/icon'></a>
    <a href="https://gitlab.com/AlexeyRudkovskiy/eda/commits/iOS"><img alt="tests report" src="http://194.110.6.97:2323/job/ProjectFood/lastStableBuild/testReport?text=tests" /></a>
</p>

## API

## WEB

